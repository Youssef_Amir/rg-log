// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAIzzPen0ohKWsqt2p1McrPF3MX6GfqTts",
    authDomain: "webgen-4dee4.firebaseapp.com",
    databaseURL: "https://webgen-4dee4.firebaseio.com",
    projectId: "webgen-4dee4",
    storageBucket: "webgen-4dee4.appspot.com",
    messagingSenderId: "1050980322316",
    appId: "1:1050980322316:web:a9bffe1a1dae2914257229",
    measurementId: "G-7467CF6B8Y"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
